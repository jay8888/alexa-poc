 
'use strict';
const AWS = require('aws-sdk');
const Alexa = require("alexa-sdk");
const fetch = require('node-fetch');
const https = require('https');
const lambda = new AWS.Lambda();
const dynamoDb = new AWS.DynamoDB.DocumentClient();
const uuid = require('uuid');
let country;
let topic;
let city;

var Amadeus = require('amadeus');
const amadeusClientId = process.env.AMADEUS_CLIENT_ID;
const amadeusClientSecret = process.env.AMADEUS_CLIENT_SECRET;
var amadeus = new Amadeus({
  clientId: amadeusClientId,
  clientSecret: amadeusClientSecret
});

const googleMapsKey = process.env.GOOGLE_KEY;
const googleMapsClient = require('@google/maps').createClient({
  key: googleMapsKey,
  rate: {limit: 50},
  Promise: Promise
});

exports.handler = function(event, context, callback) {
  const alexa = Alexa.handler(event, context);
  alexa.appId = "amzn1.ask.skill.caf40334-96e3-4297-b483-c7b3222b461f";
  alexa.registerHandlers(handlers);
  alexa.execute();
};

async function getTravelAdvice(country) {
  // Default options are marked with *
  let url = '/api/content/foreign-travel-advice/'+country.toLowerCase();
  const response = await fetch("https://www.gov.uk"+url, {
    method: 'GET', // *GET, POST, PUT, DELETE, etc.
    mode: 'cors', // no-cors, *cors, same-origin
    cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
    credentials: 'same-origin', // include, *same-origin, omit
    headers: {
      'Content-Type': 'application/json'
      // 'Content-Type': 'application/x-www-form-urlencoded',
    },
    redirect: 'follow', // manual, *follow, error
    referrer: 'no-referrer', // no-referrer, *client
  });
  return await response.json(); // parses JSON response into native JavaScript objects
}

async function getPOI(cityPar, countryPar){
  city = cityPar;
  country = countryPar;
  let location;
  let lat;
  let lng;
  let POIs = "";
  if (city){
      location = city;
  } else
  if (country){
      location = country;
  }
  console.log("LOCATION",location);
  await googleMapsClient.geocode({
      address: location
  })
  .asPromise()
  .then((response) => {
      lat = response.json.results[0].geometry.location.lat;
      lng = response.json.results[0].geometry.location.lng;
  })
  .catch((err) => {
      console.log(err);
  });
  
  console.log(lat+","+lng);
  let POIData = await amadeus.referenceData.locations.pointsOfInterest.get({
      latitude : lat,
      longitude : lng
  })
  console.log(JSON.stringify(POIData));
  for (let i=0;i<POIData.result.data.length;i++){
      POIs = POIs.concat(", " + POIData.result.data[i].name);
  }
  POIs = POIs.replace("&","and")
  console.log(POIs);
  const speakOutput = `Places to visit in ` + location + ` include ` + POIs;
  //Test version of API so only select locations available, e.g. London, New York
  return speakOutput;
}

const handlers = {
  'LaunchRequest': function () {
    this.emit('Prompt');
  },
  'Unhandled': function () {
    this.emit('AMAZON.HelpIntent');
  },
  'GetTravelAdviceIntent': function () {
    country = this.event.request.intent.slots.country.value;
    if((typeof(country) != "undefined")){
      this.emit('InfoPrompt')
    }else{
      this.emit('NoMatch')
    }
  },
  'GetTopicIntent': function () {
    topic = this.event.request.intent.slots.topic.resolutions.resolutionsPerAuthority[0].values[0].value.name;
    if((typeof(topic) != "undefined")){
      this.emit('SpeakInfo')
    }else{
      this.emit('NoMatch')
    }
  },
  'GetPOIIntent': async function () {
    city = this.event.request.intent.slots.city.value;
    country = this.event.request.intent.slots.country.value;
    let speakOutput = await getPOI(city, country);
    if((typeof(city) != "undefined") || (typeof(country) != "undefined")){
      this.emit(':ask', speakOutput)
    }else{
      this.emit('NoMatch')
    }
  },
  'AMAZON.YesIntent': function () {
    this.emit('Prompt');
  },
  'AMAZON.NoIntent': function () {
    this.emit('AMAZON.StopIntent');
  },
  'Prompt': function () {
    this.emit(':ask', 'Welcome to Travel Guru. Do you want travel advice for a country or things to do in a location?', 'Please say that again?');
  },
  'InfoPrompt': function () {
    this.emit(':ask', `Please tell me what type of information you would like for ` + country, 'Please say that again?');
  },
  'SpeakInfo': async function (){
    const response = await getTravelAdvice(country);
    let answer ='';
    let speakOutput = '';
    answer = response.details.parts.filter(function (el) {
        return (el.slug === topic.replace(/\s+/g, '-'));
    });
    if (answer.length === 0){ //Unfortunately the gov.uk API does not have all topics available for all countries
        speakOutput = `Information on this topic is unavailable for the country ${country}`
    } else {
        speakOutput = answer[0].body.replace(/<\/?[^>]+>/gi, '');
    }
    this.emit(':ask', speakOutput);
  },
  'NoMatch': function () {
    this.emit(':ask', 'Sorry, I could not understand.', 'Please say that again?');
  },
  'AMAZON.HelpIntent': function () {
    const speechOutput = 'Sorry I could not understand that. Please repeat.';
    const reprompt = 'Say hello, to hear me speak.';

    this.response.speak(speechOutput).listen(reprompt);
    this.emit(':responseReady');
  },
  'AMAZON.CancelIntent': function () {
    this.response.speak('Goodbye!');
    this.emit(':responseReady');
  },
  'AMAZON.StopIntent': function () {
    this.response.speak('See you later!');
    this.emit(':responseReady');
  }
};